package newsparser;

import newsparser.strategies.StrategyExecutor;

public class NewsCrawler {
    public static void main(String[] args) {
        StrategyExecutor.executeParseStrategy();
    }
}
