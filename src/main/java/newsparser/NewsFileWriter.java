package newsparser;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class NewsFileWriter {

    public static final String DELIMITER = "==========================================================================";
    public static final String PARSE_URL = "src\\main\\resources\\filesDb\\news.txt";

    private NewsFileWriter() {
    }

    public static synchronized void writeFile(String path, List<Article> list) {
        if (list.isEmpty()) {
            throw new IllegalStateException("The list of articles is empty!");
        }
        try (FileWriter file = new FileWriter(path, true);
             PrintWriter output = new PrintWriter(file)) {
            output.println(list.get(0).getHomePage());
            output.println(DELIMITER);
            for (Article value : list) {
                output.println(value.toString());
            }
        } catch (IOException e) {
            System.out.println("Exception occurred while writing a file");
        }
    }

    public static void clearFile(String path) {
        try {
            new FileWriter(path, false).close();
        } catch (IOException e) {
            System.out.println("Exception occurred while truncating a file");
        }
    }
}
