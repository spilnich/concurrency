package newsparser;

public class Article {
    public static final String STRING = "\r\n\t\t";
    private String title;
    private String url;
    private String description;
    private String image;
    private String source;
    private String date;
    private String homePage;

    public void setUrl(String url) {
        this.url = url;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHomePage() {
        return homePage;
    }

    public void setHomePage(String homePage) {
        this.homePage = homePage;
    }

    @Override
    public String toString() {
        return "Article{" +
                (((title == null) || (title.equals(""))) ? "" : "title='" + title + '\'' + STRING) +
                (((url == null) || (url.equals(""))) ? "" : "url='" + url + '\'' + STRING) +
                (((description == null) || (description.equals(""))) ? "" : "content='" + description + '\'' + STRING) +
                (((image == null) || (image.equals(""))) ? "" : "image='" + image + '\'' + STRING) +
                (((source == null) || (source.equals(""))) ? "" : "source='" + source + '\'' + STRING) +
                (((date == null) || (date.equals(""))) ? "" : "date='" + date + '\'') +
                '}' + System.lineSeparator();
    }
}
