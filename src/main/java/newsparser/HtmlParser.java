package newsparser;

import newsparser.strategies.Parsing;

public class HtmlParser {
    private final Parsing parsing;

    public HtmlParser(Parsing parsing) {
        this.parsing = parsing;
    }

    public void getParsedArticles() {
        parsing.parseArticles();
    }
}
