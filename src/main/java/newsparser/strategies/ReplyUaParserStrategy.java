package newsparser.strategies;

import newsparser.Article;
import newsparser.ArticleType;
import newsparser.NewsFileWriter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReplyUaParserStrategy implements Parsing {

    @Override
    public List<Article> parseArticles() {
        List<Article> articleList = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            try {
                Document document = getDocument(i);

                Elements elements = document.getElementsByClass("shortstory cf");

                if (isEmpty(elements)) break;

                getElements(articleList, elements);
            } catch (IOException e) {
                System.out.println("Parsing REPLY_UA site went wrong");
            }
        }
        NewsFileWriter.writeFile(NewsFileWriter.PARSE_URL, articleList);
        return articleList;
    }

    private void getElements(List<Article> articleList, Elements elements) {
        for (Element element : elements) {

            String url = element.getElementsByClass("short_title").first()
                    .getElementsByTag("a").attr("href");
            String image = element.getElementsByTag("a").first().attr("style");
            String title = element.getElementsByClass("short_title").first()
                    .getElementsByTag("a").text();
            String description = element.getElementsByClass("short-story_post").text();

            Article article = new Article();
            article.setTitle(title);
            article.setUrl(url);
            article.setDescription(description);
            article.setImage(image);
            article.setHomePage(ArticleType.REPLY_UA.name());

            articleList.add(article);
        }
    }

    private boolean isEmpty(Elements elements) {
        return elements.isEmpty();
    }

    private Document getDocument(int page) throws IOException {
        String replyUaUrl = "https://replyua.net/sport/page/%d/";
        return Jsoup.connect(String.format(replyUaUrl, page))
                .userAgent("Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 " +
                        "(KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36")
                .referrer("https://replyua.net/")
                .get();
    }
}
