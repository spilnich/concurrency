package newsparser.strategies;

import newsparser.Article;
import newsparser.ArticleType;
import newsparser.NewsFileWriter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UkrNetParserStrategy implements Parsing {

    @Override
    public List<Article> parseArticles() {
        List<Article> articleList = new ArrayList<>();

        while (articleList.size() != 100) {
            try {
                Document document = getDocument();

                Elements elements = document.getElementsByClass("im");

                if (elements.isEmpty()) break;

                getElements(articleList, elements);
            } catch (IOException e) {
                System.out.println("Parsing UKR_NET site went wrong");
            }
        }
        NewsFileWriter.writeFile(NewsFileWriter.PARSE_URL, articleList);
        return articleList;
    }

    private void getElements(List<Article> articleList, Elements elements) {
        for (Element element : elements) {

            String url = element.select("div.im-tl > a").attr("href");
            String date = element.getElementsByClass("im-tm").first().text();
            String title = element.getElementsByClass("im-tl").first()
                    .getElementsByTag("a").text();
            String source = element.getElementsByClass("im-pr").first().text();

            Article article = new Article();
            article.setTitle(title);
            article.setUrl(url);
            article.setDate(date);
            article.setSource(source);
            article.setHomePage(ArticleType.UKR_NET.name());

            articleList.add(article);
        }
    }

    private Document getDocument() throws IOException {
        String ukrNetUrl = "https://www.ukr.net/news/tehnologii.html";
        return Jsoup.connect(ukrNetUrl)
                .userAgent("Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 " +
                        "(KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36")
                .referrer("https://www.ukr.net/")
                .get();
    }
}
