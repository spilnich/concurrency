package newsparser.strategies;

import newsparser.Article;
import newsparser.ArticleType;
import newsparser.NewsFileWriter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FourPDAParserStrategy implements Parsing {

    public static final String ITEMPROP = "itemprop";

    @Override
    public List<Article> parseArticles() {
        List<Article> articleList = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            try {
                Document document = getDocument(i);

                Elements elements = document.getElementsByClass("post");

                if (isEmpty(elements)) break;

                getElements(articleList, elements);
            } catch (IOException e) {
                System.out.println("Parsing 4PDA site went wrong");
            }
        }
        NewsFileWriter.writeFile(NewsFileWriter.PARSE_URL, articleList);
        return articleList;
    }

    private void getElements(List<Article> articleList, Elements elements) {
        for (Element element : elements) {

            String title = element.getElementsByAttributeValue(ITEMPROP, "name").text();
            String url = element.getElementsByAttributeValue("rel", "bookmark").attr("href");
            String description = element.getElementsByAttributeValue(ITEMPROP, "description").text();
            String image = element.getElementsByAttributeValue(ITEMPROP, "image").attr("src");
            String date = element.getElementsByClass("date").text();

            Article article = new Article();
            article.setTitle(title);
            article.setUrl(url);
            article.setDescription(description);
            article.setImage(image);
            article.setDate(date);
            article.setHomePage(ArticleType.FOUR_PDA.name());

            articleList.add(article);
        }
    }

    private boolean isEmpty(Elements elements) {
        return elements.isEmpty();
    }

    private Document getDocument(int page) throws IOException {
        String fourPdaUrl = "https://4pda.ru/page/%d";
        return Jsoup.connect(String.format(fourPdaUrl, page))
                .userAgent("Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 " +
                        "(KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36")
                .referrer("")
                .get();
    }
}
