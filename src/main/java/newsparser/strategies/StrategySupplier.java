package newsparser.strategies;

import newsparser.ArticleType;

public class StrategySupplier {

    public Parsing getStrategy(ArticleType value) {

        switch (value) {
            case FOUR_PDA:
                return new FourPDAParserStrategy();
            case UKR_NET:
                return new UkrNetParserStrategy();
            case REPLY_UA:
                return new ReplyUaParserStrategy();
            default:
                throw new IllegalStateException("Unexpected value: " + value);
        }
    }
}
