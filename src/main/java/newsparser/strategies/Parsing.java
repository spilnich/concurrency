package newsparser.strategies;

import newsparser.Article;

import java.util.List;

public interface Parsing {

    List<Article> parseArticles();
}
