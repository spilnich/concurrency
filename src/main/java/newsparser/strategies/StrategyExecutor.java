package newsparser.strategies;

import newsparser.ArticleType;
import newsparser.HtmlParser;
import newsparser.NewsFileWriter;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class StrategyExecutor {

    private StrategyExecutor() {
    }

    private static final ExecutorService service = Executors.newFixedThreadPool(2);
    private static final StrategySupplier strategySupplier = new StrategySupplier();

    public static void executeParseStrategy() {

        NewsFileWriter.clearFile(NewsFileWriter.PARSE_URL);

        Arrays.stream(ArticleType.values())
                .forEach(value -> service.submit(new HtmlParser(strategySupplier.getStrategy(value))::getParsedArticles));

        service.shutdown();
    }
}