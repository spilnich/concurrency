package newsparser.strategies;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class FourPDAParserStrategyTest {

    private FourPDAParserStrategy fourPDAParserStrategy;
    private ReplyUaParserStrategy replyUaParserStrategy;
    private UkrNetParserStrategy ukrNetParserStrategy;

    @BeforeEach
    void setUp() {
        fourPDAParserStrategy = new FourPDAParserStrategy();
        replyUaParserStrategy = new ReplyUaParserStrategy();
        ukrNetParserStrategy = new UkrNetParserStrategy();
    }

    @Test
    @DisplayName("Should return not null parsed list of articles")
    void parseArticles() {
        assertNotNull(fourPDAParserStrategy.parseArticles());
        assertNotNull(replyUaParserStrategy.parseArticles());
        assertNotNull(ukrNetParserStrategy.parseArticles());
    }
}